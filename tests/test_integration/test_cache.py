# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest

from equilibrator_cache import Q_, Compound, R, Reaction, default_T


@pytest.fixture(scope="module")
def atp_comp(ccache):
    return ccache.get_compound("KEGG:C00002")


@pytest.fixture(scope="module")
def atp_hydrolysis_reaction(ccache) -> Reaction:
    formula = "KEGG:C00002 + KEGG:C00001 <=> KEGG:C00008 + KEGG:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.mark.parametrize(
    ("accession", "exists", "first_name", "first_accession"),
    [
        ("KEGG:C00001", True, "H2O", "metanetx.chemical:MNXM2"),
        ("KEGG:C00002", True, "ATP", "metanetx.chemical:MNXM3"),
        (
            "KEGG:C00003",
            True,
            "Nicotinamide adenine dinucleotide",
            "metanetx.chemical:MNXM8",
        ),
        (
            "KEGG:C00004",
            True,
            "Nicotinamide adenine dinucleotide - reduced",
            "metanetx.chemical:MNXM10",
        ),
        ("KEGG:C99999", False, None, None),
        ("CHEBI:17925", True, "Alpha-D-glucose", "metanetx.chemical:MNXM99"),
    ],
)
def test_get_compound(ccache, accession, exists, first_name, first_accession):
    c = ccache.get_compound(accession)
    if exists:
        assert c is not None
        assert c.get_common_name() == first_name
        assert c.get_accession() == first_accession
    else:
        assert c is None


@pytest.mark.parametrize(
    ("inchi", "inchi_key"),
    [
        ("InChI=1S/p+1", "GPRLSGONYQIRFK-UHFFFAOYSA"),
        ("InChI=1S/H2O/h1H2", "XLYOFNOQVPJJNP-UHFFFAOYSA"),
        (
            "InChI=1S/CH2O3/c2-1(3)4/h(H2,2,3,4)/p-1",
            "BVKZGUZCCUSVTD-UHFFFAOYSA",
        ),
        ("InChI=1S/O2/c1-2", "MYMOFIZGZYHOMD-UHFFFAOYSA"),
    ],
)
def test_get_compound_by_inchi(ccache, inchi, inchi_key):
    c = ccache.get_compound_by_inchi(inchi)
    assert c is not None

    c_list = ccache.search_compound_by_inchi_key(inchi_key)
    assert c in c_list


@pytest.mark.parametrize(
    ("accession", "is_proton", "is_water"),
    [
        ("KEGG:C00080", True, False),
        ("KEGG:C00001", False, True),
        ("KEGG:C00060", False, False),
    ],
)
def test_water_and_protons(ccache, accession, is_proton, is_water):
    cpd = ccache.get_compound(accession)
    assert ccache.is_proton(cpd) == is_proton
    assert ccache.is_water(cpd) == is_water


@pytest.mark.parametrize(
    ("query", "expected", "score"),
    [
        ("ATP", "ZKHQWZAMYRWXGA-KQYNXXCUSA-J", 1.0),
        ("water", "XLYOFNOQVPJJNP-UHFFFAOYSA-N", 1.0),
    ],
)
def test_search(ccache, query, expected, score):
    compounds = ccache.search(query)
    assert len(compounds) > 0
    compound, match = compounds[0]
    # The search should have a perfect match.
    assert match == score
    assert compound.inchi_key == expected


def test_atp_microspecies(atp_comp):
    expected_ddg = [
        0.0,
        -72.3,
        -114.2,
        -143.2,
        -162.1,
        -171.0,
        -176.2,
    ]  # kJ/mol
    major_ms = 2

    ms_without_mg = [
        ms for ms in atp_comp.microspecies if ms.number_magnesiums == 0
    ]

    assert len(ms_without_mg) == len(expected_ddg)

    expected_ddg = Q_(expected_ddg, "kJ/mol")
    expected_ddg -= expected_ddg[major_ms]

    RT = R * default_T
    for ms, ddg in zip(ms_without_mg, expected_ddg):
        assert ms.ddg_over_rt == pytest.approx((ddg / RT).m_as(""), rel=0.5)


@pytest.mark.parametrize(
    ("p_h, ionic_strength, temperature, p_mg, expected"),
    [
        list(map(Q_, ["5.0", "0.0 M", "300K", "10.0", "371.64 kJ/mol"])),
        list(map(Q_, ["7.0", "0.0 M", "300K", "10.0", "521.47 kJ/mol"])),
        list(map(Q_, ["9.0", "0.0 M", "300K", "10.0", "662.23 kJ/mol"])),
        list(map(Q_, ["7.0", "0.5 M", "300K", "10.0", "520.37 kJ/mol"])),
    ],
)
def test_atp_formation(
    p_h, ionic_strength, temperature, p_mg, expected, atp_comp
):
    assert type(atp_comp) == Compound

    ddg = atp_comp.transform(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )
    assert ddg.m_as("kJ/mol") == pytest.approx(
        expected.m_as("kJ/mol"), rel=1e-3
    )


@pytest.mark.parametrize(
    ("p_h", "ionic_strength", "temperature", "p_mg", "expected"),
    [
        list(map(Q_, ["5.0", "0.0 M", "300K", "10.0", "-39.66 kJ/mol"])),
        list(map(Q_, ["7.0", "0.0 M", "300K", "10.0", "-41.66 kJ/mol"])),
        list(map(Q_, ["9.0", "0.0 M", "300K", "10.0", "-51.68 kJ/mol"])),
        list(map(Q_, ["7.0", "0.5 M", "300K", "10.0", "-38.88 kJ/mol"])),
    ],
)
def test_atp_hydrolysis(
    p_h, ionic_strength, temperature, p_mg, expected, atp_hydrolysis_reaction
):
    assert type(atp_hydrolysis_reaction) == Reaction

    ddg = atp_hydrolysis_reaction.transform(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )
    assert ddg.m_as("kJ/mol") == pytest.approx(
        expected.m_as("kJ/mol"), rel=1e-3
    )
