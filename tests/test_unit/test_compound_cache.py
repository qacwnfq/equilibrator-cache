# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest

from equilibrator_cache.compound_cache import CompoundCache
from equilibrator_cache.models import Compound, CompoundIdentifier, Registry


@pytest.fixture(scope="module")
def mini_kegg_cache(engine):
    cache = CompoundCache(engine)

    compounds = [
        {
            "id": 1,
            "inchi_key": "XTWYTFMLZFPYCI-KQYNXXCUSA-K",
            "atom_bag": {"C": 10, "H": 12, "N": 5, "O": 10, "P": 2},
        },
        {
            "id": 2,
            "inchi_key": "ZKHQWZAMYRWXGA-KQYNXXCUSA-J",
            "atom_bag": {"C": 10, "H": 12, "N": 5, "O": 13, "P": 3},
        },
        {
            "id": 3,
            "inchi_key": "WQZGKKKJIJFFOK-GASJEMHNSA-N",
            "atom_bag": {"C": 6, "H": 12, "O": 6},
        },
        {
            "id": 4,
            "inchi_key": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "atom_bag": {"H": 2, "O": 1},
        },
        {
            "id": 5,
            "inchi_key": "LFQSCWFLJHTTHZ-UHFFFAOYSA-N",
            "atom_bag": {"C": 2, "H": 6, "O": 1},
        },
        {
            "id": 6,
            "inchi_key": "UDMBCSSLTHHNCD-KQYNXXCUSA-L",
            "atom_bag": {"C": 10, "H": 12, "N": 5, "O": 7, "P": 1},
        },
        {
            "id": 7,
            "inchi_key": "NBIIXXVUZAFLBC-UHFFFAOYSA-L",
            "atom_bag": {"H": 1, "O": 4, "P": 1},
        },
    ]
    cache.session.bulk_insert_mappings(Compound, compounds)

    identifiers = [
        {"compound_id": 1, "registry_id": 1, "accession": "C00008"},
        {"compound_id": 2, "registry_id": 1, "accession": "C00002"},
        {"compound_id": 3, "registry_id": 1, "accession": "C00031"},
        {"compound_id": 4, "registry_id": 1, "accession": "C00001"},
        {"compound_id": 5, "registry_id": 1, "accession": "C00469"},
        {"compound_id": 6, "registry_id": 1, "accession": "C00020"},
        {"compound_id": 7, "registry_id": 1, "accession": "C00009"},
        {"compound_id": 4, "registry_id": 2, "accession": "water"},
        {"compound_id": 4, "registry_id": 2, "accession": "H2O"},
        {"compound_id": 1, "registry_id": 2, "accession": "ADP"},
        {"compound_id": 2, "registry_id": 2, "accession": "ATP"},
    ]
    cache.session.bulk_insert_mappings(CompoundIdentifier, identifiers)

    registries = [
        {"id": 1, "name": "KEGG", "namespace": "kegg", "pattern": r"C^\d+$"},
        {"id": 2, "name": "Synonyms", "namespace": "synonyms", "pattern": ""},
    ]
    cache.session.bulk_insert_mappings(Registry, registries)
    cache.session.commit()
    return cache


@pytest.mark.parametrize("kwargs", [{}])
def test___init__(engine, kwargs):
    CompoundCache(engine=engine, **kwargs)


def test_all_compound_accessions(mini_kegg_cache):
    expected = [
        "ADP",
        "ATP",
        "C00001",
        "C00002",
        "C00008",
        "C00009",
        "C00020",
        "C00031",
        "C00469",
        "H2O",
        "water",
    ]
    assert mini_kegg_cache.all_compound_accessions() == expected
    expected.reverse()
    assert mini_kegg_cache.all_compound_accessions(ascending=False) == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("C00001", True),
        ("C00008", True),
        ("C00020", True),
        ("C00301", False),
        ("C00666", False),
        (None, False),
    ],
)
def test_accession_exists(mini_kegg_cache, accession, expected):
    assert mini_kegg_cache.accession_exists(accession) == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("kegg:C00001", {"H": 2, "O": 1}),
        ("kegg:C00008", {"C": 10, "H": 12, "N": 5, "O": 10, "P": 2}),
        ("kegg:C00002", {"C": 10, "H": 12, "N": 5, "O": 13, "P": 3}),
    ],
)
def test_get_element_data_frame(mini_kegg_cache, accession, expected):
    compound = mini_kegg_cache.get_compound(accession)
    assert compound is not None
    elements = mini_kegg_cache.get_element_data_frame([compound])
    assert elements[compound].to_dict() == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("kegg:C00001", ["water", "H2O"]),
        ("kegg:C00008", ["ADP"]),
        ("kegg:C00002", ["ATP"]),
    ],
)
def test_get_compound_names(mini_kegg_cache, accession, expected):
    compound = mini_kegg_cache.get_compound(accession)
    assert compound is not None
    assert mini_kegg_cache.get_compound_names(compound).issuperset(expected)
    assert compound.get_common_name() == expected[0]


@pytest.mark.parametrize(
    "namespace, identifiers, is_inchi_key, total",
    [
        ("", [], False, 0),
        ("kegg", [], False, 0),
        ("kegg", ["C99999"], False, 0),
        ("kegg", ["C00001"], False, 1),
        ("kegg", ["C00001", "C00002", "C00008"], False, 3),
        ("kegg", ["C00001", "C00002", "C00001"], False, 2),
        (
            "inchikey",
            ["WQZGKKKJIJFFOK-GASJEMHNSA-N", "XTWYTFMLZFPYCI-KQYNXXCUSA-K"],
            True,
            2,
        ),
    ],
)
def test_get_compounds(
    mini_kegg_cache, namespace, identifiers, is_inchi_key, total
):
    results = mini_kegg_cache.get_compounds(
        namespace, identifiers, is_inchi_key=is_inchi_key
    )
    assert len(results) == total
